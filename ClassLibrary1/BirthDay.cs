﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectParty
{
    public class BirthDay : Party
    {
        string[] _nameOfGames = new string[5] { "Paper boat race", "Scavenger Hunt", "Air Hockey", "Ball In Bucket", "Jenga Game" };
        string[] _nameOfEvent = new string[2] { "Magic Show", "Nail Art" };
        const int CostPerEvents = 5000;
        const int CostPerGames = 1000;
        int _noOfAddOnGames = 0;
        int _noOfAddOnEvents = 0;
        int _totalAmountForGames = 0;
        int _totalAmountForEvents = 0;
        public BirthDay()
        {
        }
        public int NoOfAddOnGames
        {
            get { return _noOfAddOnGames; }
            set { _noOfAddOnGames = value; }
        }
        public int NoOfAddOnEvents
        {
            get { return _noOfAddOnEvents; }
            set { _noOfAddOnEvents = value; }
        }
        public int TotalAmountForGames
        {
            get { return _totalAmountForGames; }
            set { _totalAmountForGames = value; }
        }
        public int TotalAmountForEvents
        {
            get { return _totalAmountForEvents; }
            set { _totalAmountForEvents = value; }
        }
                public void EnterUserDetails()
        {
            UserMobileNo = "21212";
            Console.Write("Enter your name:");
            UserName = Console.ReadLine();
            Console.WriteLine();
            while (UserMobileNo.Length != 10)
            {
                Console.Write("Enter the your Mobile Number:");
                UserMobileNo = Console.ReadLine();
                if (UserMobileNo.Length != 10)
                {
                    Console.WriteLine();
                    Console.Write("Enter the 10 Digit Mobile Number:");
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.Write("Enter the your Address:");
            UserAddress = Console.ReadLine();
        }
        public void EnterPartyDetails()
        {
            EnterUserDetails();
            Console.Write("Enter the venue:");
            Venue = Console.ReadLine();
            Console.WriteLine();
            Console.Write("Enter the venue Address:");
            VenueAddress = Console.ReadLine();
            Console.WriteLine();
            try
            {
                Console.Write("Enter a date (eg. 01/12/2020): ");
                string pattern = "dd/mm/yyyy";
                DateTime date = DateTime.ParseExact(Console.ReadLine(), pattern, null);
                PartyDate = date.ToString(pattern);
                date = date.AddDays(2);
                BillingDate = date.ToString(pattern);
            }
            catch (Exception e)
            {
                Console.WriteLine("You have Provided the wrong input");
                PartyDate = "02/03/2020";
                BillingDate = "04/03/2020";
                ///   string pattern = "dd/mm/yyyy";
                //   DateTime date = DateTime.Today;
                //    PartyDate = date.ToString(pattern);
                //   date = date.AddDays(2);
                //  BillingDate = date.ToString(pattern);
                Console.WriteLine("You Party Date is " + PartyDate);
            }
            Console.WriteLine();
            Person:
            Console.Write("Enter the Number Of Persons:");
            string str = Console.ReadLine();
            bool success = int.TryParse(str, out int noPerson);
            if (noPerson < 0)
            {
                Console.WriteLine("!Provide the Posite Number Only");
                goto Person;
            }
            if (success)
            {
                NoOfPerson = noPerson;
            }
            else
            {
                Console.WriteLine("!Provide the correct Input:Only Numbers");
                goto Person;
            }
            Catering = (Catering * NoOfPerson);
            Console.WriteLine();
            Console.WriteLine("congratulations your party is registered with us");
            Console.Clear();
            Console.WriteLine("Hope you enjoyed party");
        }

        public void SilverPackage()
        {
            string checkYesOrNo = "a";
            int flag = 0;
            Catering = 600;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 12000;
            Console.WriteLine("Decoration:" + Decoration);
            Console.WriteLine("Games:");
            for (int indexOfGames = 0; indexOfGames <= 1; indexOfGames++)
            {
                Console.WriteLine("->" + _nameOfGames[indexOfGames]);
            }
            Console.WriteLine("-->Add Ons");
            Console.WriteLine("Games");
            for (int indexOfGames = 2; indexOfGames <= 4; indexOfGames++)
            {
                Console.WriteLine("->" + _nameOfGames[indexOfGames]);
            }
            Console.WriteLine("Events");
            for (int indexOfEvents = 0; indexOfEvents <= 1; indexOfEvents++)
            {
                Console.WriteLine("->" + _nameOfEvent[indexOfEvents]);
            }
            Console.WriteLine("Charges Per Game:" + CostPerGames);
            Console.WriteLine("Charges Per Events:" + CostPerEvents);

            while (flag != 1 && flag != 2)
            {
                Console.Write("Do you want to opt. Any Add Ons: Input yes or no");
                Console.WriteLine();
                checkYesOrNo = Console.ReadLine();
                checkYesOrNo = checkYesOrNo.ToLower();
                if (checkYesOrNo == "yes")
                {
                    AddOnGame:
                    flag = 1;
                    Console.Write("Enter the Number of game as Add On (0 to 3):");
                    string str = Console.ReadLine();
                    bool success = int.TryParse(str, out int noOfGames);
                    if (noOfGames <= -1 || noOfGames >= 4)
                    {
                        Console.WriteLine("!Provide the correct Input 0,1,2,3");
                        goto AddOnGame;
                    }
                    if (success)
                    {
                        NoOfAddOnGames = noOfGames;
                        Console.WriteLine();
                        AddOnEvent:
                        Console.Write("Enter the Number of Events as Add On (0 to 2):");
                        str = Console.ReadLine();
                        bool innerSuccess = int.TryParse(str, out int noOfEvent);
                        if (noOfEvent <= -1 || noOfEvent >= 3)
                        {
                            Console.WriteLine("!Provide the correct Input 0,1,2");
                            goto AddOnEvent;
                        }
                        if (innerSuccess)
                        {
                            NoOfAddOnEvents = noOfEvent;
                        }
                        else
                        {
                            Console.WriteLine("!Provide the correct Input 0,1,2");
                            goto AddOnEvent;
                        }
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input 0,1,2,3");
                        goto AddOnGame;
                    }
                }
                else if (checkYesOrNo == "no")
                {
                    flag = 2;
                    _noOfAddOnGames = 0;
                    _noOfAddOnEvents = 0;
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input yes or no:");
                }
            }
            Console.WriteLine();
            EnterPartyDetails();
        }
        public void GoldPackage()
        {
            Console.Clear();
            string checkYesOrNo;
            Catering = 1000;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 18000;
            Console.WriteLine("Decoration:" + Decoration);
            Console.WriteLine("Games:");
            for (int indexOfGames = 0; indexOfGames <= 2; indexOfGames++)
            {
                Console.WriteLine("->" + _nameOfGames[indexOfGames]);
            }
            Console.WriteLine("-->Add Ons");
            Console.WriteLine("Games");
            for (int indexOfGames = 3; indexOfGames <= 4; indexOfGames++)
            {
                Console.WriteLine("->" + _nameOfGames[indexOfGames]);
            }
            Console.WriteLine("Events");
            for (int indexOfEvents = 0; indexOfEvents <= 1; indexOfEvents++)
            {
                Console.WriteLine("->" + _nameOfEvent[indexOfEvents]);
            }
            Console.WriteLine("Charges Per Game:" + CostPerGames);
            Console.WriteLine("Charges Per Events:" + CostPerEvents);
            int flag = 0;
            while (flag != 1 && flag != 2)
            {
                Console.Write("Do you want to opt. Any Add Ons: Input yes or no:");
                checkYesOrNo = Console.ReadLine();
                Console.WriteLine();
                checkYesOrNo = checkYesOrNo.ToLower();
                if (checkYesOrNo == "yes")
                {
                    AddOnGame:
                    flag = 1;
                    Console.Write("Enter the Number of game as Add On (0 to 2):");
                    string str = Console.ReadLine();
                    bool success = int.TryParse(str, out int noOfGames);
                    if (noOfGames <= -1 || noOfGames >= 3)
                    {
                        Console.WriteLine("!Provide the correct Input 0,1,2");
                        goto AddOnGame;
                    }
                    if (success)
                    {
                        NoOfAddOnGames = noOfGames;
                        Console.WriteLine();
                        AddOnEvent:
                        Console.Write("Enter the Number of Events as Add On (0 to 2):");
                        str = Console.ReadLine();
                        bool innerSuccess = int.TryParse(str, out int noOfEvent);
                        if (noOfEvent <= -1 || noOfEvent >= 3)
                        {
                            Console.WriteLine("!Provide the correct Input 0,1,2");
                            goto AddOnEvent;
                        }
                        if (innerSuccess)
                        {
                            NoOfAddOnEvents = noOfEvent;
                        }
                        else
                        {
                            Console.WriteLine("!Provide the correct Input 0,1,2");
                            goto AddOnEvent;
                        }
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input 0,1,2");
                        goto AddOnGame;
                    }
                }
                else if (checkYesOrNo == "no")
                {
                    flag = 2;
                    _noOfAddOnGames = 0;
                    _noOfAddOnEvents = 0;
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input yes or no:");
                }
            }
            Console.WriteLine();
            EnterPartyDetails();
        }
        public void PlataniumPackage()
        {
            Console.Clear();
            Catering = 1800;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 35000;
            Console.WriteLine("Decoration:" + Decoration);
            Console.WriteLine("Games:");
            for (int indexOfGames = 0; indexOfGames <= 4; indexOfGames++)
            {
                Console.WriteLine("->" + _nameOfGames[indexOfGames]);
            }
            Console.WriteLine("Events");
            for (int indexOfEvents = 0; indexOfEvents <= 1; indexOfEvents++)
            {
                Console.WriteLine("->" + _nameOfEvent[indexOfEvents]);
            }
            EnterPartyDetails();
        }
        public override void CalculateTotalAmount()
        {
            Console.Clear();
            PartyMAxTime = 3;
            Console.WriteLine("Hope you enjoyed party");
            Console.WriteLine("Please provide me the following details:");
            PartyStart:
            Console.Write("Enter the party start time (only:HH):");
            string str = Console.ReadLine();
            bool success = int.TryParse(str, out int partySt);
            if (partySt<1)
            {
                Console.WriteLine("!Provide the correct Input");
                goto PartyStart;
            }
            if (success)
            {
                PartyStartTime = partySt;
            }
            else
            {
                Console.WriteLine("!Provide the correct Input");
                goto PartyStart;
            }
            Console.WriteLine();
            PartyStop:
            Console.Write("Enter the party stop time (only:HH):");
            str = Console.ReadLine();
            bool success1 = int.TryParse(str, out int partySp);
            if (partySp < 1)
            {
                Console.WriteLine("!Provide the correct Input");
                goto PartyStop;
            }
            if (success)
            {
                PartyStopTime = partySp;
            }
            else
            {
                Console.WriteLine("!Provide the correct Input");
                goto PartyStop;
            }
            Console.WriteLine();
            CalculateExtraTime();
            Console.Write("Extra Time Spend:" + ExtraTimeSpend);
            Console.WriteLine();
            Console.WriteLine("Press any Key to Continue");
            Console.ReadKey();
            Console.Clear();
            TotalAmountForGames = NoOfAddOnGames * CostPerGames;
            TotalAmountForEvents = NoOfAddOnEvents * CostPerEvents;
            if (NoOfPerson > 0)
            {
                TotalExpenditure = (Catering * NoOfPerson) + Decoration + TotalAmountForGames + TotalAmountForEvents;
                CostPerPerson = TotalExpenditure / NoOfPerson;
            }
            else
            {
                TotalExpenditure = (Catering) + Decoration + TotalAmountForGames + TotalAmountForEvents;
                CostPerPerson = TotalExpenditure / NoOfPerson;
            }            
            Console.WriteLine();
            Console.WriteLine("---------Successive Tech.Event Orgniser---------");
            Console.WriteLine();
            Console.Write("Billing Date:" + BillingDate);
            Console.Write("  Party Date:" + PartyDate);
            Console.WriteLine();
            Console.Write("Host Name:" + UserName);
            Console.WriteLine();
            Console.Write("Event:"+EventType);
            Console.Write("          Package Type:" + PackageType);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("    Amount Paid For Catering     :" + Catering);
            Console.WriteLine("    Amount Paid For Decoration   :" + Decoration);
            Console.WriteLine("    Extra Amount Paid For Games  :" + TotalAmountForGames);
            Console.WriteLine("    Extra Amount Paid For Events :" + TotalAmountForEvents);
                        Console.WriteLine("    Amount Per Person            :" + CostPerPerson);
            Console.WriteLine("    Total Amount                 :" + TotalExpenditure);
            //
            Console.WriteLine();
            Console.Write("Enter file name in which you want save bill:");
            string fileName = Console.ReadLine();
            fileName = fileName + ".txt";
            string textPath1 = @"C:\Users\ayusha\Documents\Bill\Birthday\";
            DirectoryInfo di = Directory.CreateDirectory(textPath1);
            textPath1 = textPath1 + fileName;
            Console.WriteLine("File Path:" + textPath1);
            StreamWriter sw = File.CreateText(textPath1);
            sw.WriteLine();
            sw.WriteLine("---------Successive Tech.Event Orgniser---------");
            sw.WriteLine();
            sw.Write("Billing Date:" + BillingDate);
            sw.Write("  Party Date:" + PartyDate);
            sw.WriteLine();
            sw.Write("Host Name:" + UserName);
            sw.WriteLine();
            sw.Write("Event:" + EventType);
            sw.Write("          Package Type:" + PackageType);
            sw.WriteLine();
            sw.WriteLine();
            sw.WriteLine("    Amount Paid For Catering     :" + Catering);
            sw.WriteLine("    Amount Paid For Decoration   :" + Decoration);
            sw.WriteLine("    Extra Amount Paid For Games  :" + TotalAmountForGames);
            sw.WriteLine("    Extra Amount Paid For Events :" + TotalAmountForEvents);
            sw.WriteLine("    Amount Per Person            :" + CostPerPerson);
            sw.WriteLine("    Total Amount                 :" + TotalExpenditure);
            sw.Close();
            //
            Console.WriteLine("Press the key to go back to Package Menu");
            Console.ReadKey();
        }
    }
}
