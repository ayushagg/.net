﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectParty
{
    public class Party
    {
        int _partyMAxTime;
        int _catering;
        int _decoration;
        string _venue;
        string _venueAddress;
        string _partyDate;
        string _billingDate;
        int _partyStartTime;
        int _partyStopTime;
        int _extraTimeSpend;
        string _eventType;
        string _packageType;
        int _costPerPerson;
        int _totalExpenditure;
        int _noOfPerson;
        string _userName;
        string _userMobileNo;
        string _userAddress;
        public Party()
        { }
        public int PartyMAxTime
        {
            get { return _partyMAxTime; }
            set { _partyMAxTime = value; }
        }
        public int Catering
        {
            get { return _catering; }
            set { _catering = value; }
        }
        public int Decoration
        {
            get { return _decoration; }
            set { _decoration = value; }
        }
        public string Venue
        {
            get { return _venue; }
            set { _venue = value; }
        }
        public string VenueAddress
        {
            get { return _venueAddress; }
            set { _venueAddress = value; }
        }
        public string PartyDate
        {
            get { return _partyDate; }
            set { _partyDate = value; }
        }
        public string BillingDate
        {
            get { return _billingDate; }
            set { _billingDate = value; }
        }
        public int PartyStartTime
        {
            get { return _partyStartTime; }
            set { _partyStartTime = value; }
        }

            public int PartyStopTime
        {
            get { return _partyStopTime; }
            set { _partyStopTime = value; }
        }
        public int ExtraTimeSpend
        {
            get { return _extraTimeSpend; }
            set { _extraTimeSpend = value; }
        }
        public string PackageType
        {
            get { return _packageType; }
            set { _packageType = value; }
        }
        public string EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }
        public int CostPerPerson
        {
            get { return _costPerPerson; }
            set { _costPerPerson = value; }
        }
        public int TotalExpenditure
        {
            get { return _totalExpenditure; }
            set { _totalExpenditure = value; }
        }
        public int NoOfPerson
        {
            get { return _noOfPerson; }
            set { _noOfPerson = value; }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string UserMobileNo
        {
            get { return _userMobileNo; }
            set { _userMobileNo = value; }
        }
        public string UserAddress
        {
            get { return _userAddress; }
            set { _userAddress = value; }
        }
        public virtual void CalculateTotalAmount()
        {
            if (NoOfPerson == 0)
            {
                TotalExpenditure = (Catering * NoOfPerson) + Decoration;
                CostPerPerson = 0;
            }
            else
            {
                TotalExpenditure = (Catering * NoOfPerson) + Decoration;
                CostPerPerson = TotalExpenditure / NoOfPerson;
            }
        }

        
        public void CalculateExtraTime()
        {
            int durationOfParty=(PartyStopTime-PartyStartTime);
            ExtraTimeSpend = durationOfParty-PartyMAxTime;
            if (ExtraTimeSpend < 0)
            {
                ExtraTimeSpend = 0;
            }
        }
    }
}
