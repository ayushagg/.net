﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectParty
{
    public class BabyShower : Party
    {
        string _theme;
        public string Theme
        {
            get { return _theme; }
            set { _theme = value; }
        }

        public void EnterUserDetails()
        {
            UserMobileNo = "21212";
            Console.Write("Enter your name:");
            UserName = Console.ReadLine();
            Console.WriteLine();
            while (UserMobileNo.Length != 10)
            {
                Console.Write("Enter the your Mobile Number:");
                UserMobileNo = Console.ReadLine();
                if (UserMobileNo.Length != 10)
                {
                    Console.WriteLine();
                    Console.Write("Enter the 10 Digit Mobile Number:");
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.Write("Enter the your Address:");
            UserAddress = Console.ReadLine();
        }
        public void EnterPartyDetails()
        {
            EnterUserDetails();
            Console.Write("Enter the venue:");
            Venue = Console.ReadLine();
            Console.WriteLine();
            Console.Write("Enter the venue Address:");
            VenueAddress = Console.ReadLine();
            Console.WriteLine();
            try
            {
                Console.Write("Enter a date (eg. 01/12/2020): ");
                string pattern = "dd/mm/yyyy";
                DateTime date = DateTime.ParseExact(Console.ReadLine(), pattern, null);
                PartyDate = date.ToString(pattern);
                date = date.AddDays(2);
                BillingDate = date.ToString(pattern);
            }
            catch (Exception e)
            {
                Console.WriteLine("You have Provided the wrong input");
                PartyDate = "02/03/2020";
                BillingDate = "04/03/2020";
                Console.WriteLine("You Party Date is " + PartyDate);
            }
            Console.WriteLine();
            Person:
            Console.Write("Enter the Number Of Persons:");
            string str = Console.ReadLine();
            bool success = int.TryParse(str, out int noPerson);
            if (noPerson < 0)
            {
                Console.WriteLine("!Provide the Posite Number Only");
                goto Person;
            }
            if (success)
            {
                NoOfPerson = noPerson;
            }
            else
            {
                Console.WriteLine("!Provide the correct Input:Only Numbers");
                goto Person;
            }
            Catering = (Catering * NoOfPerson);
            Console.WriteLine();
            Console.WriteLine("congratulations your party is registered with us");
            Console.Clear();
            Console.WriteLine("Hope you enjoyed party");
        }
        public void SilverPackage()
        {
            Console.Clear();
            string checkChoice = "a";
            int flag = 0;
            Catering = 1200;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 15000;
            Console.WriteLine("Decoration:" + Decoration);
            while (flag != 1 && flag != 2)
            {
                Console.WriteLine("Themes:");
                Console.WriteLine("->Prince Theme");
                Console.WriteLine("->Princess Theme");
                Console.WriteLine("Enter the Theme(e.g.princess): ");
                checkChoice = Console.ReadLine();
                checkChoice = checkChoice.ToLower();
                if (checkChoice == "prince")
                {
                    flag = 1;
                    Theme = checkChoice;
                    Console.WriteLine("You have choosen " + Theme + " Theme");
                }
                else if (checkChoice == "princess")
                {
                    flag = 2;
                    Theme = checkChoice;
                    Console.WriteLine("You have choosen " + Theme + " Theme");
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input prince or princess:");
                }
            }
            EnterPartyDetails();
        }
        public void GoldPackage()
        {
            Console.Clear();
            string checkChoice = "a";
            int flag = 0;
            Catering = 2000;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 25000;
            Console.WriteLine("Decoration:" + Decoration);
            while (flag != 1 && flag != 2)
            {
                Console.WriteLine("Themes:");
                Console.WriteLine("->Prince Theme");
                Console.WriteLine("->Princess Theme");
                Console.WriteLine("Enter the Theme(e.g.princess): ");
                checkChoice = Console.ReadLine();
                checkChoice = checkChoice.ToLower();
                if (checkChoice == "prince")
                {
                    flag = 1;
                    Theme = checkChoice;
                    Console.WriteLine("You have choosen " + Theme + " Theme");
                }
                else if (checkChoice == "princess")
                {
                    flag = 2;
                    Theme = checkChoice;
                    Console.WriteLine("You have choosen " + Theme + " Theme");
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input prince or princess:");
                }
            }
            EnterPartyDetails();
        }
        public void PlataniumPackage()
        {
            Console.Clear();
            string checkChoice = "a";
            int flag = 0;
            Catering = 2500;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 35000;
            Console.WriteLine("Decoration:" + Decoration);
            while (flag != 1 && flag != 2)
            {
                Console.WriteLine("Themes:");
                Console.WriteLine("->Prince Theme");
                Console.WriteLine("->Princess Theme");
                Console.WriteLine("Enter the Theme(e.g.princess): ");
                checkChoice = Console.ReadLine();
                checkChoice = checkChoice.ToLower();
                if (checkChoice == "prince")
                {
                    flag = 1;
                    Theme = checkChoice;
                    Console.WriteLine("You have choosen " + Theme + " Theme");
                }
                else if (checkChoice == "princess")
                {
                    flag = 2;
                    Theme = checkChoice;
                    Console.WriteLine("You have choosen " + Theme + " Theme");
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input prince or princess:");
                }
            }
            EnterPartyDetails();
        }
        public override void CalculateTotalAmount()
        {
            Console.Clear();
            PartyMAxTime = 4;
            Console.WriteLine("Hope you enjoyed party");
            Console.WriteLine("Please provide me the following details:");
            Console.WriteLine("Enter the party start time (only:HH):");
            PartyStartTime = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the party stop time (only:HH):");
            PartyStopTime = int.Parse(Console.ReadLine());
            Console.WriteLine("Extra Time Spend:" + ExtraTimeSpend);
            Console.WriteLine("Press any key to continue..");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("---------Successive Tech.Event Orgniser---------");
            Console.WriteLine();
            Console.Write("Billing Date:" + BillingDate);
            Console.Write("  Party Date:" + PartyDate);
            Console.WriteLine();
            Console.Write("Host Name:" + UserName);
            Console.WriteLine();
            Console.Write("Event:" + EventType);
            Console.Write("          Package Type:" + PackageType);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("    Amount Paid For Catering     :" + Catering);
            Console.WriteLine("    Amount Paid For Decoration   :" + Decoration);
            if (NoOfPerson > 0)
            {
                TotalExpenditure = (NoOfPerson * Catering) + Decoration;
                CostPerPerson = TotalExpenditure / NoOfPerson;
            }
            else
            {
                TotalExpenditure =Catering + Decoration;
                CostPerPerson = 0;
            }
            Console.WriteLine("    Amount Per Person            :" + CostPerPerson);
            Console.WriteLine("    Total Amount                 :" + TotalExpenditure);
            //
            Console.WriteLine();
            Console.Write("Enter file name in which you want save bill:");
            string fileName = Console.ReadLine();
            fileName = fileName + ".txt";
            string textPath1 = @"C:\Users\ayusha\Documents\Bill\Babyshower\";
            DirectoryInfo di = Directory.CreateDirectory(textPath1);
            textPath1 = textPath1 + fileName;
            Console.WriteLine("File Path:" + textPath1);
            StreamWriter sw = File.CreateText(textPath1);
            sw.WriteLine();
            sw.WriteLine("---------Successive Tech.Event Orgniser---------");
            sw.WriteLine();
            sw.Write("Billing Date:" + BillingDate);
            sw.Write("  Party Date:" + PartyDate);
            sw.WriteLine();
            sw.Write("Host Name:" + UserName);
            sw.WriteLine();
            sw.Write("Event:" + EventType);
            sw.Write("          Package Type:" + PackageType);
            sw.WriteLine();
            sw.WriteLine();
            sw.WriteLine("    Amount Paid For Catering     :" + Catering);
            sw.WriteLine("    Amount Paid For Decoration   :" + Decoration);
            sw.WriteLine("    Amount Per Person            :" + CostPerPerson);
            sw.WriteLine("    Total Amount                 :" + TotalExpenditure);
            sw.WriteLine("Press the key to go back to Package Menu");
            sw.Close();
            //
            Console.ReadKey();
        }
    }
}
