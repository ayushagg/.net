﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectParty
{
    public class Sangeet : Party
    {
        int _noOfDancer;
        int _noOfChoregrapher;
        int _totalAmountForDancer;
        int _totalAmountForChoregrapher;
        const int DancerCharge = 500;
        const int ChoregrapherCharge = 5000;
        public Sangeet()
        {
        }
        public int NoOfDancer
        {
            get { return _noOfDancer; }
            set { _noOfDancer = value; }
        }
        public int NoOfChoregrapher
        {
            get { return _noOfChoregrapher; }
            set { _noOfChoregrapher = value; }
        }
        public int TotalAmountForChoregrapher
        {
            get { return _totalAmountForChoregrapher; }
            set { _totalAmountForChoregrapher = value; }
        }
        public int TotalAmountForDancer
        {
            get { return _totalAmountForDancer; }
            set { _totalAmountForDancer = value; }
        }
        public void EnterUserDetails()
        {
            UserMobileNo = "21212";
            Console.Write("Enter your name:");
            UserName = Console.ReadLine();
            Console.WriteLine();
            while (UserMobileNo.Length != 10)
            {
                Console.Write("Enter the your Mobile Number:");
                UserMobileNo = Console.ReadLine();
                if (UserMobileNo.Length != 10)
                {
                    Console.WriteLine();
                    Console.Write("Enter the 10 Digit Mobile Number:");
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.Write("Enter the your Address:");
            UserAddress = Console.ReadLine();
        }
        public void EnterPartyDetails()
        {
            EnterUserDetails();
            Console.Write("Enter the venue:");
            Venue = Console.ReadLine();
            Console.WriteLine();
            Console.Write("Enter the venue Address:");
            VenueAddress = Console.ReadLine();
            Console.WriteLine();
            try
            {
                Console.Write("Enter a date (eg. 01/12/2020): ");
                string pattern = "dd/mm/yyyy";
                DateTime date = DateTime.ParseExact(Console.ReadLine(), pattern, null);
                PartyDate = date.ToString(pattern);
                date = date.AddDays(2);
                BillingDate = date.ToString(pattern);
            }
            catch (Exception e)
            {
                Console.WriteLine("You have Provided the wrong input");
                PartyDate = "02/03/2020";
                BillingDate = "04/03/2020";
                ///   string pattern = "dd/mm/yyyy";
                //   DateTime date = DateTime.Today;
                //    PartyDate = date.ToString(pattern);
                //   date = date.AddDays(2);
                //  BillingDate = date.ToString(pattern);
                Console.WriteLine("You Party Date is " + PartyDate);
            }
            Console.WriteLine();
            Person:
            Console.Write("Enter the Number Of Persons:");
            string str = Console.ReadLine();
            bool success = int.TryParse(str, out int noPerson);
            if (noPerson < 0)
            {
                Console.WriteLine("!Provide the Posite Number Only");
                goto Person;
            }
            if (success)
            {
                NoOfPerson = noPerson;
            }
            else
            {
                Console.WriteLine("!Provide the correct Input:Only Numbers");
                goto Person;
            }
            Catering = (Catering * NoOfPerson);
            Console.WriteLine();
            Console.WriteLine("congratulations your party is registered with us");
            Console.Clear();
            Console.WriteLine("Hope you enjoyed party");
        }

        public void SilverPackage()
        {
            Console.Clear();
            string checkYesOrNo = "a";
            int flag = 0;
            Catering = 1200;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 15000;
            Console.WriteLine("Decoration:" + Decoration);
            Console.WriteLine("-->Add Ons");
            Console.WriteLine("Choregrapher");
            Console.WriteLine("Dancer");
            while (flag != 1 && flag != 2)
            {
                Console.WriteLine("Do you want to opt. Any Add Ons: Input yes or no");
                checkYesOrNo = Console.ReadLine();
                checkYesOrNo = checkYesOrNo.ToLower();
                if (checkYesOrNo == "yes")
                {
                    AddOnChoregraoher:
                    flag = 1;
                    Console.WriteLine("Enter the Number of Choregrapher:");
                    string str = Console.ReadLine();
                    bool success = int.TryParse(str, out int noOfChoreg);
                    if (noOfChoreg <= -1)
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnChoregraoher;
                    }
                    if (success)
                    {
                        NoOfChoregrapher = noOfChoreg;
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnChoregraoher;
                    }
                    AddOnDancer:
                    Console.WriteLine("Enter the Number of Dancer:");
                    str = Console.ReadLine();
                    success = int.TryParse(str, out int noOfDancer);
                    if (noOfDancer <= -1)
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnDancer;
                    }
                    if (success)
                    {
                        NoOfDancer = noOfDancer;
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnDancer;
                    }
                }
                else if (checkYesOrNo == "no")
                {
                    flag = 2;
                    NoOfChoregrapher = 0;
                    NoOfDancer = 0;
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input yes or no:");
                }
            }
            EnterPartyDetails();
        }
        public void GoldPackage()
        {
            Console.Clear();
            string checkYesOrNo = "a";
            int flag = 0;
            Catering = 1300;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 18000;
            Console.WriteLine("Decoration:" + Decoration);
            Console.WriteLine("One Choregrapher is included in Gold Package");
            Console.WriteLine("-->Add Ons");
            Console.WriteLine("Choregrapher");
            Console.WriteLine("Dancer");
            while (flag != 1 && flag != 2)
            {
                Console.WriteLine("Do you want to opt. Any Add Ons: Input yes or no");
                checkYesOrNo = Console.ReadLine();
                checkYesOrNo = checkYesOrNo.ToLower();
                if (checkYesOrNo == "yes")
                {
                    AddOnChoregraoher:
                    flag = 1;
                    Console.WriteLine("Enter the Number of Choregrapher:");
                    string str = Console.ReadLine();
                    bool success = int.TryParse(str, out int noOfChoreg);
                    if (noOfChoreg <= -1)
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnChoregraoher;
                    }
                    if (success)
                    {
                        NoOfChoregrapher = noOfChoreg;
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnChoregraoher;
                    }
                    AddOnDancer:
                    Console.WriteLine("Enter the Number of Dancer:");
                    str = Console.ReadLine();
                    success = int.TryParse(str, out int noOfDancer);
                    if (noOfDancer <= -1)
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnDancer;
                    }
                    if (success)
                    {
                        NoOfDancer = noOfDancer;
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnDancer;
                    }
                }
                else if (checkYesOrNo == "no")
                {
                    flag = 2;
                    NoOfChoregrapher = 0;
                    NoOfDancer = 0;
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input yes or no:");
                }
            }
            EnterPartyDetails();
        }
        public void PlataniumPackage()
        {
            Console.Clear();
            string checkYesOrNo = "a";
            int flag = 0;
            Catering = 1800;
            Console.WriteLine("Catering:" + Catering);
            Decoration = 25000;
            Console.WriteLine("Decoration:" + Decoration);
            Console.WriteLine("1 Choregrapher & 4 Background Dancers are included in Gold Package");
            Console.WriteLine("-->Add Ons");
            Console.WriteLine("Choregrapher");
            Console.WriteLine("Dancer");
            while (flag != 1 && flag != 2)
            {
                Console.WriteLine("Do you want to opt. Any Add Ons: Input yes or no");
                checkYesOrNo = Console.ReadLine();
                checkYesOrNo = checkYesOrNo.ToLower();
                if (checkYesOrNo == "yes")
                {
                    AddOnChoregraoher:
                    flag = 1;
                    Console.WriteLine("Enter the Number of Choregrapher:");
                    string str = Console.ReadLine();
                    bool success = int.TryParse(str, out int noOfChoreg);
                    if (noOfChoreg <= -1)
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnChoregraoher;
                    }
                    if (success)
                    {
                        NoOfChoregrapher = noOfChoreg;
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnChoregraoher;
                    }
                    AddOnDancer:
                    Console.WriteLine("Enter the Number of Dancer:");
                    str = Console.ReadLine();
                    success = int.TryParse(str, out int noOfDancer);
                    if (noOfDancer <= -1)
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnDancer;
                    }
                    if (success)
                    {
                        NoOfDancer = noOfDancer;
                    }
                    else
                    {
                        Console.WriteLine("!Provide the correct Input");
                        goto AddOnDancer;
                    }
                }
                else if (checkYesOrNo == "no")
                {
                    flag = 2;
                    NoOfChoregrapher = 0;
                    NoOfDancer = 0;
                }
                else
                {
                    Console.WriteLine("Please Provide the correct Input yes or no:");
                }
            }
            EnterPartyDetails();
        }
        public override void CalculateTotalAmount()
        {
            Console.Clear();
            PartyMAxTime = 5;
            Console.WriteLine("Hope you enjoyed party");
            Console.WriteLine("Please provide me the following details:");
            Console.WriteLine("Enter the party start time (only:HH):");
            PartyStartTime = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the party stop time (only:HH):");
            PartyStopTime = int.Parse(Console.ReadLine());
            Console.WriteLine("Extra Time Spend:" + ExtraTimeSpend);
            Console.WriteLine("Press any key to continue......");
            Console.ReadKey();
            Console.Clear();
            TotalAmountForChoregrapher = NoOfChoregrapher * ChoregrapherCharge;
            TotalAmountForDancer = NoOfDancer * DancerCharge;
            if (NoOfPerson > 0)
            {
                TotalExpenditure = (Catering * NoOfPerson) + Decoration + TotalAmountForChoregrapher + TotalAmountForDancer;
                CostPerPerson = TotalExpenditure / NoOfPerson;
            }
            else
            {
                TotalExpenditure = Catering + Decoration + TotalAmountForChoregrapher + TotalAmountForDancer;
                CostPerPerson = 0;
            }
            Console.WriteLine("---------Successive Tech.Event Orgniser---------");
            Console.WriteLine();
            Console.Write("Billing Date:" + BillingDate);
            Console.Write("  Party Date:" + PartyDate);
            Console.WriteLine();
            Console.Write("Host Name:" + UserName);
            Console.WriteLine();
            Console.Write("Event:" + EventType);
            Console.Write("          Package Type:" + PackageType);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("    Amount Paid For Catering  :" + Catering);
            Console.WriteLine("    Amount Paid For Decoration:" + Decoration);
            Console.WriteLine("    Extra Amount Paid For:");
            Console.WriteLine("    Choregrapher              :" + TotalAmountForChoregrapher);
            Console.WriteLine("    Dancer:" + TotalAmountForDancer);
            Console.WriteLine("    Amount Per Person         :" + CostPerPerson);
            Console.WriteLine("    Total Amount              :" + TotalExpenditure);
            //
            Console.WriteLine();
            Console.Write("Enter file name in which you want save bill:");
            string fileName = Console.ReadLine();
            fileName = fileName + ".txt";
            string textPath1 = @"C:\Users\ayusha\Documents\Bill\Sangeet\";
            DirectoryInfo di = Directory.CreateDirectory(textPath1);
            textPath1 = textPath1 + fileName;
            Console.WriteLine("File Path:" + textPath1);
            StreamWriter sw = File.CreateText(textPath1);
            sw.WriteLine();
            sw.WriteLine("---------Successive Tech.Event Orgniser---------");
            sw.WriteLine();
            sw.Write("Billing Date:" + BillingDate);
            sw.Write("  Party Date:" + PartyDate);
            sw.WriteLine();
            sw.Write("Host Name:" + UserName);
            sw.WriteLine();
            sw.Write("Event:" + EventType);
            sw.Write("          Package Type:" + PackageType);
            sw.WriteLine();
            sw.WriteLine();
            sw.WriteLine("    Amount Paid For Catering  :" + Catering);
            sw.WriteLine("    Amount Paid For Decoration:" + Decoration);
            sw.WriteLine("    Extra Amount Paid For:");
            sw.WriteLine("    Choregrapher              :" + TotalAmountForChoregrapher);
            sw.WriteLine("    Dancer:" + TotalAmountForDancer);
            sw.WriteLine("    Amount Per Person         :" + CostPerPerson);
            sw.WriteLine("    Total Amount              :" + TotalExpenditure);
            sw.Close();
            //
            Console.WriteLine("Press the key to go back to Package Menu");
            Console.ReadKey();
        }
    }
}
