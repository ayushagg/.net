﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtensionMethod;

namespace DelegatePrac
{
    public  class AddSub
    {
        public string name{get;set; }
          // Declaring the delegate 
        public delegate void addnum(int a, int b);
        public delegate void subnum(int a, int b);
        public delegate int my_delegate(int s, int d,
                                  int f, int g);
        public delegate bool my_delegate1(string mystring);

        public delegate void petName(string pet);

        // method "sum" 
        public void  sum(int a, int b)
        {
            Console.WriteLine("({0} + {1}) = {2}", a, b, a + b);
        }

        // method "subtract" 
        public void subtract(int a, int b)
        {
            Console.WriteLine("({0} - {1}) = {2}", a, b, a - b);
        }
        public static void myfun(int p, int q)
        {
            Console.WriteLine(p - q);
        }

        public static int multiplication(int s, int d,
                           int f, int g)
        {
            return s * d * f * g;
        }

        public static bool checkStringLength(string mystring)
        {
            if (mystring.Length < 7)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        // Anonymous type object is passed in the  
        // method which has dynamic type parameters 
        static public void userInfo(dynamic val)
        {
            Console.WriteLine(val.s_id);
            Console.WriteLine(val.s_name);
            Console.WriteLine(val.language);
        }

        static void Main(string[] args)
        {
            AddSub objAddSub = new AddSub();

            addnum del_objAddNum = new addnum(objAddSub.sum);
            subnum del_objSubNum = new subnum(objAddSub.subtract);
            del_objAddNum(20, 10);
            del_objSubNum(20, 10);

            //Action Delegate
            Action<int, int> val = myfun;
            val(20, 5);

            //Function Delegate
            my_delegate obj = multiplication;
            Console.WriteLine(obj(2, 2, 2, 2));


            my_delegate1 objCheckStringLength = checkStringLength;
            Console.WriteLine(objCheckStringLength("Hello"));

            //Predicate Delegate
            Predicate<string> objPredicate = checkStringLength;
            Console.WriteLine(objPredicate("Ayush Aggarwal"));

            // Anonymous type object 
            var anony_object = new
            {
                s_id = 251,
                s_name = "AYUSH AGGARWAL",
                language = "c#"
            };

            // Calling mymethod 
            userInfo(anony_object);

            string fav = "Rabbit";

            // Anonymous method with one parameter 
            petName p = delegate (string mypet)
            {
                Console.WriteLine("My favorite pet is {0}.", mypet);
                // Accessing variable defined 
                // outside the anonymous function 
                Console.WriteLine("And I like {0} also.", fav);
            };
            p("Parrot");



            //1) Create Extension methods for DateTime and Nullable DateTime 
            //to print date in following formats:DD / MM / YY HH: MM
            // DD.MM.YY HH: MM
            //DD.MM.YY HH: MM: SS
            //DD / MM / YY HH: MM: SS

            DateTime date = DateTime.Now;
            Console.WriteLine(date.GetTime());
            Console.WriteLine(date.GetTimeDot());
            Console.WriteLine(date.GetTimeDotSecond());
            Console.WriteLine(date.GetTimeSecond());


            //2) Create Extension methods for String and Objects 
            //and it should return parsed int value if successfully parses 
            //else returns null

            string name = "Ayush";
            int? result=name.CheckStringIsParsed();
            if (result == null)
            {
                Console.WriteLine("null");
            }
            else
            {
                Console.WriteLine(result);
            }

            name = "11";
            result = name.CheckStringIsParsed();
            if (result == null)
            {
                Console.WriteLine("null");
            }
            else
            {
                Console.WriteLine(result);
            }

            AddSub objAddSub1 = new AddSub();
            objAddSub1.name ="Try";

            result = objAddSub1.CheckStringIsParsedObj();
            if (result == null)
            {
                Console.WriteLine("null");
            }
            else
            {
                Console.WriteLine(result);
            }

            objAddSub1.name = "15";
            result = objAddSub1.CheckStringIsParsedObj();
            if (result == null)
            {
                Console.WriteLine("null");
            }
            else
            {
                Console.WriteLine(result);
            }


            //3) Create Extension methods for String and Objects and 
            //it should return parsed int value if successfully parses 
            //else returns default value which you will pass in the method.

            objAddSub1.name = "Ayush";
            Console.WriteLine(objAddSub1.CheckStringIsParsedObjIfNotReturnDefault());

            objAddSub1.name = "15";
            Console.WriteLine(objAddSub1.CheckStringIsParsedObjIfNotReturnDefault());
            Console.ReadKey();
        }
    }
}
