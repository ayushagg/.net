﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
namespace ProjectParty
{
    class Program
    {
        public Program()
        {
        }
        static void Main(string[] args)
        {
            int flag = 0;
            int choice = 0;
            while (choice != 4)
            {
                Console.Clear();
                Console.WriteLine("Successive Tech. Event Orgniser");
                Console.WriteLine("1.Birth day");
                Console.WriteLine("2.Baby Shower");
                Console.WriteLine("3.Sangeet");
                Console.WriteLine("4.Exit");
                Console.WriteLine("Select The Event:");
                if (flag == 1)
                {
                    Console.WriteLine("!Provide the correct input[1,2,3,4]");
                    flag = 0;
                }
                Console.Write("Enter your Choice [1,2,3,4]:");

                var str = Console.ReadLine();
                bool success = int.TryParse(str, out choice);
                if (success)
                {
                    switch (choice)
                    {
                        case 1:
                            int innerSwitchChoice = 0;
                            while (innerSwitchChoice != 4)
                            {
                                Console.Clear();
                                Console.WriteLine("Successive Tech. Event Orgniser");
                                Console.WriteLine("Event:Birthday");
                                Console.WriteLine("1.Silver Package");
                                Console.WriteLine("2.Gold Package");
                                Console.WriteLine("3.Platanium Package");
                                Console.WriteLine("4.Exit");
                                Console.WriteLine("Please Select the package:");
                                if (flag == 1)
                                {
                                    Console.WriteLine("!Provide the correct input[1,2,3,4]");
                                    flag = 0;
                                }
                                Console.Write("Enter your Choice [1,2,3,4]:");
                                str = Console.ReadLine();
                                success = int.TryParse(str, out innerSwitchChoice);
                                if (success)
                                {
                                    switch (innerSwitchChoice)
                                    {
                                        case 1:
                                            BirthDay objBirthDay = new BirthDay();
                                            objBirthDay.EventType = "Birth Day";
                                            objBirthDay.PackageType = "Silver";
                                            objBirthDay.SilverPackage();
                                            objBirthDay.CalculateTotalAmount();
                                            break;
                                        case 2:
                                            BirthDay objBirthDay1 = new BirthDay();
                                            objBirthDay1.EventType = "Birth Day";
                                            objBirthDay1.PackageType = "Gold";
                                            objBirthDay1.GoldPackage();
                                            objBirthDay1.CalculateTotalAmount();
                                            break;
                                        case 3:
                                            BirthDay objBirthDay2 = new BirthDay();
                                            objBirthDay2.EventType = "Birth Day";
                                            objBirthDay2.PackageType = "Platanium";
                                            objBirthDay2.PlataniumPackage();
                                            objBirthDay2.CalculateTotalAmount();
                                            break;
                                        default:
                                            Console.WriteLine("Please enter the correct Choice");
                                            break;
                                    }
                                }

                                else
                                {
                                    flag = 1;
                                }
                            }
                            break;
                        case 2:
                            innerSwitchChoice = 0;
                            while (innerSwitchChoice != 4)
                            {
                                Console.Clear();
                                Console.WriteLine("Successive Tech. Event Orgniser");
                                Console.WriteLine("Event:Baby Shower");
                                Console.WriteLine("1.Silver Package");
                                Console.WriteLine("2.Gold Package");
                                Console.WriteLine("3.Platanium Package");
                                Console.WriteLine("4.Exit");
                                Console.WriteLine("Please Select the package:");
                                if (flag == 1)
                                {
                                    Console.WriteLine("!Provide the correct input[1,2,3,4]");
                                    flag = 0;
                                }
                                str = Console.ReadLine();
                                success = int.TryParse(str, out innerSwitchChoice);
                                if (success)
                                {

                                    switch (innerSwitchChoice)
                                    {
                                        case 1:
                                            BabyShower objBabyShower = new BabyShower();
                                            objBabyShower.EventType = "Baby Shower";
                                            objBabyShower.PackageType = "Silver";
                                            objBabyShower.SilverPackage();
                                            objBabyShower.CalculateTotalAmount();
                                            break;
                                        case 2:
                                            BabyShower objBabyShower1 = new BabyShower();
                                            objBabyShower1.EventType = "Baby Shower";
                                            objBabyShower1.PackageType = "Gold";
                                            objBabyShower1.GoldPackage();
                                            objBabyShower1.CalculateTotalAmount();
                                            break;
                                        case 3:
                                            BabyShower objBabyShower2 = new BabyShower();
                                            objBabyShower2.EventType = "Baby Shower";
                                            objBabyShower2.PackageType = "Platanium";
                                            objBabyShower2.PlataniumPackage();
                                            objBabyShower2.CalculateTotalAmount();
                                            break;
                                        default:
                                            Console.WriteLine("Please enter the correct Choice");
                                            break;
                                    }
                                }

                                else
                                {
                                    flag = 1;
                                }
                            }
                            break;

                        case 3:
                            innerSwitchChoice = 0;
                            while (innerSwitchChoice != 4)
                            {
                                Console.Clear();
                                Console.WriteLine("Successive Tech. Event Orgniser");
                                Console.WriteLine("Event:Sangeet");
                                Console.WriteLine("1.Silver Package");
                                Console.WriteLine("2.Gold Package");
                                Console.WriteLine("3.Platanium Package");
                                Console.WriteLine("4.Exit");
                                Console.WriteLine("Please Select the package:");
                                Console.Write("Enter your Choice [1,2,3,4]:");
                                Console.WriteLine();
                                if (flag == 1)
                                {
                                    Console.WriteLine("!Provide the correct input[1,2,3,4]");
                                    flag = 0;
                                }
                                str = Console.ReadLine();
                                success = int.TryParse(str, out innerSwitchChoice);
                                if (success)
                                {
                                    switch (innerSwitchChoice)
                                    {
                                        case 1:
                                            Sangeet objSangeet = new Sangeet();
                                            objSangeet.EventType = "Sangeet";
                                            objSangeet.PackageType = "Silver";
                                            objSangeet.SilverPackage();
                                            objSangeet.CalculateTotalAmount();
                                            break;
                                        case 2:
                                            Sangeet objSangeet1 = new Sangeet();
                                            objSangeet1.EventType = "Sangeet";
                                            objSangeet1.PackageType = "Gold";
                                            objSangeet1.GoldPackage();
                                            objSangeet1.CalculateTotalAmount();
                                            break;
                                        case 3:
                                            Sangeet objSangeet2 = new Sangeet();
                                            objSangeet2.EventType = "Sangeet";
                                            objSangeet2.PackageType = "Platanium";
                                            objSangeet2.PlataniumPackage();
                                            objSangeet2.CalculateTotalAmount();
                                            break;
                                        default:
                                            Console.WriteLine("Please enter the correct Choice");
                                            break;
                                    }
                                }

                                else
                                {
                                    flag = 1;
                                }
                            }
                            break;
                        default:
                            Console.WriteLine("Please enter the correct Choice");
                            break;
                    }
                }
                else
                {
                    flag = 1;
                }
            }
        }
    }
}
