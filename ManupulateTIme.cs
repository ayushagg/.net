﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DelegatePrac;


namespace ExtensionMethod
{
   public static class ManupulateTIme
    {
        public static string GetTime(this DateTime date)
        {
            string strDate = DateTime.Now.ToString("dd/MM/yy HH:MM") ?? null;
            var result = strDate == null ? "null" : strDate;
            return result;
        }
        public static string GetTimeSecond(this DateTime date)
        {
            string strDate = DateTime.Now.ToString("dd/MM/yy HH:MM:ss") ?? null;
            var result = strDate == null ? "null" : strDate;
            return result;
        }
        public static string GetTimeDot(this DateTime date)
        {
            string strDate= DateTime.Now.ToString("dd.MM.yy HH:MM")??null;
            var result = strDate == null ? "null" : strDate;
            return result;
        }
        public static string GetTimeDotSecond(this DateTime date)
        {
            string strDate=DateTime.Now.ToString("dd.MM.yy HH:MM:ss")??null;
            var result = strDate == null ? "null" : strDate;
            return result;
        }
        public static int? CheckStringIsParsed(this string input)
        {
            int? result = null;
            int temp;
            if (!int.TryParse(input.Trim(), out temp))
            {
                result = null;
                return result;
            }
            else
            {
                return result = Int32.Parse(input);
            }
        }

        public static int? CheckStringIsParsedObj(this AddSub input)
        {
              int? result = null;
            string str = input.name;
            int temp;
            if (!int.TryParse(str, out temp))
            {
                result = null;
                return result;
            }
            else
            {
                return result = Int32.Parse(str);
            }
        }

        public static int CheckStringIsParsedIfNotReturnDefault(this string input)
        {
            int result;
            int temp;
            if (!int.TryParse(input.Trim(), out temp))
            {
                result = 0;
                return result;
            }
            else
            {
                return result = Int32.Parse(input);
            }
        }

        public static int CheckStringIsParsedObjIfNotReturnDefault(this AddSub input)
        {
            int result;
            string str = input.name;
            int temp;
            if (!int.TryParse(str, out temp))
            {
                result =0;
                return result;
            }
            else
            {
                return result = Int32.Parse(str);
            }
        }
    } 
}
