CREATE DATABASE TestDb;
DROP DATABASE IF EXISTS TestDb;

create table tblPerson (
ID int NOT NULL Primary Key,
Name nvarchar(50) NOT NULL,
Email nvarchar(50) NOT NULL,
GenderId  int
)

drop table visits;

INSERT INTO tblGender (ID,Gender) VALUES (1,'MALE');
INSERT INTO tblGender (ID,Gender) VALUES (2,'Female');
Insert into tblGender(ID,Gender) values(3,'Unkown');

INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (1,'John','j@j.com',1);
INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (2,'Marry','m@m.com',99);
INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (3,'Rahul','r@gmail.com',1);
INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (4,'Amit','amit@j.com',1);
INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (5,'Priya','pria@j.com',2);
INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (6,'Ashtha','a@a.com',2);
INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (7,'Rohan','rohan@j.com',1);
INSERT INTO tblPerson (ID,Name,Email,GenderId) VALUES (8,'Rakesh','rakesh@j.com',1);

select Name from tblPerson where GenderId=1;

Select * from tblPerson order by Name;
Select * from tblPerson order by Name desc;
Select ID from tblPerson order by Name;
Select * from tblPerson order by Id asc,Name desc;
Select * from tblPerson order by Name desc,Id asc;

Select name from tblPerson group by Name
having Count(Id)>=1;
Select name from tblPerson group by Name
having Count(Id)=0;

Select distinct GenderId from tblPerson;
Select distinct GenderId,Name from tblPerson;

Select ID from tblPerson where name like 'a%' order by Name;
Select Name from tblPerson where Name like '%h' order by Name;
Select Name from tblPerson where Name like 'a%h' order by Name;
Select Name from tblPerson where Name like 'j%n' order by Name;
Select Name from tblPerson where Name like '_o%n' order by Name;
Select Name from tblPerson where Name like '_o%' order by Name;
Select Name from tblPerson where Name like '[a-j]%' order by Name;
Select Name from tblPerson where Name like '[ar]%' order by Name;
Select Name from tblPerson where Name like '[^ar]%' order by Name;
Select Name from tblPerson where Name like '[^a-r]%' order by Name;

select * from tblPerson where GenderId in (1,99);
select * from tblPerson where GenderId in (1,99) order by Name;
select * from tblPerson where GenderId=1 or Name like 'a%' order by Name;
select * from tblPerson where GenderId=1 AND Name like 'a%' order by Name;
select * from tblPerson where not GenderId=1 or Name like 'a%' order by Name;

update tblPerson set Email='gmail.com'  where not Email  like '%j.com';

delete from tblPerson where GenderId=99;
delete from tblGender;
TRUNCATE TABLE tblGender;
ALTER TABLE tblPerson ADD PhoneNumber int;

ALTER TABLE tblPerson ADD CONSTRAINT tblPerson_GenderId_Fk
FOREIGN KEY (GenderId) REFERENCES tblGender(ID);

ALTER TABLE tblPerson ADD CONSTRAINT DF_tblPerson_GenderId
DEFAULT 3 FOR GENDERID
INSERT INTO tblPerson (ID,Name,Email) VALUES (9,'Deepak','deepak@j.com');
Select * from tblPerson;

ALTER TABLE tblPerson ADD Age int;
insert into tblPerson values(2,'Sara','s@s.com',2,-192);
delete from tblPerson where age=-192;
alter table tblPerson
Add Constraint CK_tblPerson_Age check (AGE>0 AND AGE<150);
insert into tblPerson values(2,'Sara','s@s.com',2,112);

CREATE INDEX idx_age ON tblPerson ( Age );


CREATE TABLE tblPersonal
 (
  PersonID int,
  LastName varchar(255),
  FirstName varchar(255),
  Address varchar(255),
  City varchar(255) 
);
INSERT INTO tblPersonal VALUES (1,'John','m','JK','');
INSERT INTO tblPersonal VALUES (2,'Marry','j','Null','');
INSERT INTO tblPersonal VALUES (3,'Rahul','m','Delhi','');
INSERT INTO tblPersonal VALUES (4,'Amit','j','Mumbai','');
INSERT INTO tblPersonal VALUES (5,'Priya','am','Delhi','');
INSERT INTO tblPersonal VALUES (6,'Ashtha','j','Noida','');

SELECT * FROM tblPerson p
INNER JOIN tblPersonal c ON c.PersonID = p.ID
ORDER BY Name DESC;
SELECT * FROM tblPerson p
INNER JOIN tblPersonal c ON c.PersonID = p.ID
SELECT * FROM tblPerson p
LEFT OUTER JOIN tblPersonal c ON c.PersonID = p.ID
SELECT * FROM tblPerson p
RIGHT OUTER JOIN tblPersonal c ON c.PersonID = p.ID
SELECT * FROM tblPerson p
FULL OUTER JOIN tblPersonal c ON c.PersonID = p.ID


alter table tblPersonal add FullName varchar(90);
SELECT
    LastName,FirstName,
    CONCAT(FirstName, ' ', LastName) FullName
FROM 
    tblPersonal
ORDER BY 
    FullName;


SELECT
    FirstName + SPACE(1) + LastName FullName
FROM
    tblPersonal
ORDER BY
    FirstName,
    LastName;

update tblPerson set PhoneNumber=909829009 where ID>0; 

SELECT    
PhoneNumber,Name,Email,
 REPLACE(REPLACE(PhoneNumber, '(', ''), ')', '') FormatedPhoneNumber
FROM    
 tblPerson
WHERE PhoneNumber IS NOT NULL
ORDER BY 
 ID;


SELECT 
    TRY_CONVERT(DATETIME, '2019-18-15', 102) result;
  SELECT 
    CONVERT(DATETIME, '2019-08-15', 102) result;
SELECT 
    CONVERT(DATETIME, '20190731') result;
DECLARE @dt DATETIME= '2019-12-31 14:43:35.863';
SELECT CONVERT(VARCHAR(25), @dt, 127) s1;
SELECT Month('2017/08/25') AS Month;
SELECT DAY('2017/08/25') AS DayOfMonth;


SELECT CEILING(25.75) AS CeilValue;
SELECT COUNT(ID) AS NumberOfUsers FROM tblPerson;
SELECT SIGN(255.5);

SELECT CONVERT(int, 25.65);

DECLARE @myval decimal (5, 2);  
SET @myval = 193.57;  
SELECT CAST(CAST(@myval AS varbinary(20)) AS decimal(10,5)); 
DECLARE @myval decimal (5, 2);  
SET @myval = 193.57;
SELECT CONVERT(decimal(10,5), CONVERT(varbinary(20), @myval)); 

SELECT  CAST(10.6496 AS int) as trunc1,
         CAST(-10.6496 AS int) as trunc2,
         CAST(10.6496 AS numeric) as round1,
         CAST(-10.6496 AS numeric) as round2; 

SELECT LEN('Ayush')  Length
SELECT LEFT ('Ayush Aggarwal', 2);
SELECT LEFT ('Ayush Aggarwal', 5);
SELECT RIGHT ('SQL Server 2008',4) As Release
DECLARE @myString varchar(40)= ' Get rid of five leading blanks'
SELECT 'The new string: ' + LTRIM(@myString) As Example;
DECLARE @myString varchar(40) ='Get rid of five trailing blanks '
SELECT 'The new string: '+ RTRIM (@myString) As Example;
SELECT 'Aggarwal' AS LastName,SUBSTRING('Ayush', 1, 1)As Initial;
SELECT 'Aggarwal' AS LastName,SUBSTRING('Ayush', 1, 2)As Initial;
SELECT 'Aggarwal' AS LastName,SUBSTRING('Ayush', 2, 1)As Initial;
SELECT REPLACE('SQL Server 2005','2005','2008');
SELECT REPLACE('SQL Server 2005','2005','SQL');
SELECT REPLACE('SQL Server 2005','Ayush','SQL');
SELECT REPLACE('SQL Server 2005','sql','Ayush');

SELECT CURRENT_TIMESTAMP AS current_date_time;

DECLARE 
    @local_time DATETIME,
    @utc_time DATETIME;
 
SET @local_time = GETDATE();
SET @utc_time = GETUTCDATE();
 
SELECT 
    CONVERT(VARCHAR(40), @local_time) 
        AS 'Server local time';
SELECT 
    CONVERT(VARCHAR(40), @utc_time) 
        AS 'Server UTC time'
SELECT 
    CONVERT(VARCHAR(40), DATEDIFF(hour, @utc_time, @local_time)) 
        AS 'Server time zone';
SELECT 
    PATINDEX('%ern%', 'SQL Pattern Index') position;

SELECT 
    PATINDEX('%f__ction%', 'SQL Server String Function') position;

SELECT 
    UPPER('sql') result;
STR(123.456, 6,3) result    
STR(123.456, 7,3) result
STR(123.456, 6,1) result

create table tblPerson1 (
ID int NOT NULL Primary Key,
Name nvarchar(50) NOT NULL,
Gender  int
)



WITH tblperson_CTE 
AS  
-- Define the CTE query.  
(  
    	Select * from tblPerson

)  
-- Define the outer query referencing the CTE name.  
Select * from	tblperson_CTE where GenderId>1;


CREATE VIEW vwtblPerson 
AS
Select * from tblPerson
Select * from	vwtblperson where GenderId>1;


CREATE PROCEDURE sptblperson_getDetail
AS
Select * from	tblperson;
GO;
EXEC sptblperson;

ALTER PROCEDURE sptblperson_getDetail
	-- Add the parameters for the stored procedure here
	@Name varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select * from tblPerson where name=@Name;

END
EXEC sptblperson;


Select * from tblPerson;
create procedure sptblperson_FilterIDGenderID
@ID nvarchar(30),@GenderId nvarchar(30)
as
select * from tblPerson where ID=@ID AND GenderId=@GenderId;
Go
Exec sptblperson_FilterIDGenderID 1,2;

alter procedure sptblperson_FilterIDGenderID
@ID nvarchar(30),@GenderId nvarchar(30)
as
select * from tblPerson where ID>@ID AND GenderId>@GenderId;
Go
Exec sptblperson_FilterIDGenderID 1,1;

alter procedure sptblperson_FilterIDGenderID
@ID nvarchar(30),@GenderId nvarchar(30)
as
select * from tblPerson where ID>@ID order by GenderId;
Go
Exec sptblperson_FilterIDGenderID 1,1;

CREATE FUNCTION fntblpersonFilterPeron(@GenderId int)  
RETURNS int   
AS   
BEGIN  
    DECLARE @ret int;  
    SELECT @ret = SUM(p.GenderId)   
    FROM tblperson p   
    WHERE p.GenderId > @GenderId   
        AND p.PhoneNumber = '909829009';  
     IF (@ret IS NULL)   
        SET @ret = 0;  
    RETURN @ret;  
END ;
select dbo.fntblpersonFilterPeron(1) ;

ALTER FUNCTION fntblpersonFilterPeron(@GenderId int)  
RETURNS int   
AS   
BEGIN  
    DECLARE @ret int;  
    SELECT @ret = SUM(p.GenderId)   
    FROM tblperson p   
    WHERE p.GenderId = @GenderId   
        AND p.PhoneNumber = '909829009';  
     IF (@ret IS NULL)   
        SET @ret = 0;  
    RETURN @ret;  
END ;
select dbo.fntblpersonFilterPeron(1) ;


DROP FUNCTION fntblpersonFilterPeron;

DECLARE @person_table TABLE (
    person_name VARCHAR(50) NOT NULL,
    person_email VARCHAR(MAX) ,
    person_phone VARCHAR(MAX) 
);
INSERT INTO @person_table
SELECT
    Name,Email,PhoneNumber
FROM
    tblPerson
WHERE
    ID = 1;
 
SELECT
    *
FROM
    @person_table;

CREATE FUNCTION fntblpersonFilterGender (
    @GenderId INT
)
RETURNS TABLE
AS
RETURN
    SELECT 
        Name,
        Email,
        PhoneNumber
    FROM
		tblPerson
    WHERE
        GenderId = @GenderId;


SELECT 
    * 
FROM 
   fntblpersonFilterGender(2);


SELECT
   GenderId,
    RANK() OVER (
        ORDER BY GenderId)myrank
FROM
    tblperson;


SELECT
   GenderId,
    RANK() OVER (PARTITION BY GenderId
        ORDER BY GenderId)myrank
FROM
    tblperson;

SELECT
   GenderId,Name,
    RANK() OVER (
        ORDER BY Name)myrank
FROM
    tblperson;

SELECT
   GenderId,Name,
    RANK() OVER (
        ORDER BY GenderId)myrank
FROM
    tblperson;