Declare 
@number1 SMALLINT,
@number2 SMALLINT;

SET @number1=10;
SET @number2=3;

if(@number2!=0)
Select  @number1+@number2  Addition, @number1-@number2  Substraction, 
@number1*@number2  Multiplication, @number1 /@number2  Division;
else
begin
Select  @number1+@number2  Addition, @number1-@number2  Substraction, 
@number1*@number2  Multiplication;
end;

if(@number2!=0)
if(@number1>@number2)
 while(@number1>0)
 begin
 set @number1= @number1-@number2;
 if(@number1>0)
 select @number1;
 end;

 Select Name,
 case 
 when Gender=0 then 'Female'
  when Gender=1 then 'male' 
 when Gender=2then 'other' 
   else 'unknown' end as gender
from tblperson1;

ALTER TABLE tblperson1
ADD DateOfBirth date;

UPDATE tblperson1
SET DateOfBirth = '2017/08/25'
WHERE ID=1;

UPDATE tblperson1
SET DateOfBirth = '2016/09/25'
WHERE ID=2;

UPDATE tblperson1
SET DateOfBirth = '2011/01/11'
WHERE ID=3;


UPDATE tblperson1
SET DateOfBirth = '2001/09/09'
WHERE ID=4;

Select *,CONCAT(DATEDIFF(mm,DateOfBirth,GETDATE())/12 ,' Years ',(DATEDIFF(mm,DateOfBirth,GETDATE())-(DATEDIFF(mm,DateOfBirth,GETDATE())/12)*12),' Months ', DATEDIFF(dd,DATEPART(day,DateOfBirth),DATEPART(day,GETDATE())),' Days') as Age from tblPerson1;