﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data;
using System.Windows;


namespace ConsoleApp
{
    class Program:Person
    {
        static void Main(string[] args)
        {
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //SqlCommand cmd = new SqlCommand("Select * from tblPerson",con);
            //con.Open();
            //SqlDataReader dataReader = cmd.ExecuteReader();
            //DataTable dt = new DataTable();
            //dt.Load(dataReader);
            //foreach (DataRow row in dt.Rows) 
            //{
            //    Console.WriteLine(row["ID"]);
            //    Console.WriteLine(row["Name"]);
            //}
            // dataReader.Close();
            //cmd.Dispose();
            //con.Close();
            //while (dataReader.Read())
            //{
            //    MessageBox.Show(dataReader.GetValue(0) + " - " + dataReader.GetValue(1) + " - " + dataReader.GetValue(2));
            // }
            //dataReader.Close();
            //cmd.Dispose();
            //con.Close();


            //Create Table
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //SqlCommand cmd = new SqlCommand("CREATE TABLE Person(PersonID int,PersonName varchar(150),DateOfBirth datetime);", con);
            //con.Open();
            //cmd.ExecuteNonQuery();
            //}
            // catch (Exception ex)
            // {
            //Console.WriteLine(ex);
            //}
            //finally 
            //{
            //     con.Close();
            // }


            //Insert Data into table
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //    con.Open();
            //    SqlCommand cmd = new SqlCommand("insert into Person(PersonID,PersonName,DateOfBirth) values(@id,@name,@dob)", con);
            //    cmd.Parameters.AddWithValue("@id", "4");
            //    cmd.Parameters.AddWithValue("@name", "Mike");
            //    cmd.Parameters.AddWithValue("@dob","2001/05/17");
            //    cmd.ExecuteNonQuery();

            //    Console.WriteLine("executed");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //    Console.ReadLine();
            //}
            //finally
            //{
            //    con.Close();
            //}


            //Update Data into table
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //    con.Open();

            //    SqlCommand cmd = new SqlCommand("update Person set PersonName=@name where PersonID=@id", con);
            //    cmd.Parameters.AddWithValue("@id", "4");
            //    cmd.Parameters.AddWithValue("@name", "Raghav");
            //    cmd.ExecuteNonQuery();

            //    Console.WriteLine("executed");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //}
            //finally
            //{
            //    con.Close();
            //}


            //Create stored procedure
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //    con.Open();

            //    string query =
            //       @"Create Procedure spPerson_InsertDetail
            //        (@ID int,@Name varchar(50),@dob date)
            //        as
            //        insert into Person(PersonID,PersonName,DateOfBirth) Values(@id,@Name,@dob) ";

            //    SqlCommand cmd = new SqlCommand(query, con);
            //    SqlDataReader reader = cmd.ExecuteReader();
            //    Console.WriteLine("executed");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //    Console.ReadKey();
            //}
            //finally
            //{
            //    con.Close();
            //}


            // Used Above Stored Procedure
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //    con.Open();

            //    SqlCommand cmd = new SqlCommand("spPerson_InsertDetail", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.AddWithValue("@ID", "5");
            //    cmd.Parameters.AddWithValue("@Name", "kartik");
            //    cmd.Parameters.AddWithValue("@dob", "1955/02/11");
            //    cmd.ExecuteNonQuery();

            //    Console.WriteLine("executed");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //}
            //finally
            //{
            //    con.Close();
            //}

            //Used stored procedure & Parameters
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //    con.Open();

            //    SqlCommand cmd = new SqlCommand("spPerson_GetDetail", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.AddWithValue("@ID", "1");

            //    SqlDataReader reader = cmd.ExecuteReader();
            //    DataTable dtb = new DataTable();
            //    dtb.Load(reader);
            //    Person p = new Person();
            //    List<Person> Lp = new List<Person>();

            //    foreach (DataRow row in dtb.Rows)
            //    {
            //        Lp.Add(new Person
            //        {
            //            ID = Convert.ToInt32(row["PersonID"]),
            //            name = (row["PersonName"]).ToString()
            //        });
            //    }

            //    foreach (var i in Lp)
            //    {
            //        Console.WriteLine(i.ID + " " + i.name);
            //    }

            //    Console.WriteLine("executed");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //}
            //finally
            //{
            //    con.Close();
            //}


            //Read data from stored procedure & Parameters
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //    con.Open();

            //    SqlCommand cmd = new SqlCommand("spPerson_GetDetails", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    SqlDataReader reader =cmd.ExecuteReader();
            //    DataTable dtb = new DataTable();
            //    dtb.Load(reader);
            //    Person p = new Person();
            //    List<Person> Lp = new List<Person>();

            //    foreach (DataRow row in dtb.Rows)
            //    {
            //        Lp.Add(new Person
            //        {
            //            ID = Convert.ToInt32(row["PersonID"]),
            //            name = (row["PersonName"]).ToString(),
            //            dob = Convert.ToDateTime(row["DateOfBirth"])
            //        }) ; 
            //    }

            //    foreach (var i in Lp)
            //    {
            //        Console.WriteLine(i.ID + " " + i.name+"     " + i.dob);
            //    }

            //    Console.WriteLine("executed");
            //    Console.ReadKey();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //    Console.ReadKey();
            //}
            //finally
            //{
            //    con.Close();
            //}

            //Delete Data from table
            //SqlConnection con = new SqlConnection("data source=lapr258\\sqlexpress;integrated security=false;initial catalog=testdb;user id=sa;password=Successive");
            //try
            //{
            //    con.Open();
            //    SqlCommand cmd = new SqlCommand("delete from person where Personid=@id", con);
            //    cmd.Parameters.AddWithValue("@id","4");
            //    cmd.ExecuteNonQuery();
            //    Console.WriteLine("executed");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //    Console.ReadKey();
            //}
            //finally
            //{
            //    con.Close();
            //}


            //Read data of Stored procedure Out in c#
            //SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            //try
            //{
            //    con.Open();

            //    SqlCommand cmd = new SqlCommand("spPerson_GetDetailOP", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.Add("@dob",SqlDbType.VarChar,20);
            //    cmd.Parameters["@dob"].Direction = ParameterDirection.Output;
            //    cmd.ExecuteNonQuery();
            //    Console.WriteLine("Date of birth of person ID 1:"+cmd.Parameters["@dob"].Value);
            //    Console.WriteLine("executed");
            //    Console.ReadKey();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //    Console.ReadKey();
            //}
            //finally
            //{
            //    con.Close();
            //}


            // Use SQLCommandBuilder
            //Create a connection object  
            SqlConnection con = new SqlConnection("data source=LAPR258\\SQLEXPRESS;integrated security=false;Initial Catalog=TestDb;User ID=sa;Password=Successive");
            try
            {
                con.Open();

                // Create a data adapter object  
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Person", con);

                // Ceate a command builder object  
                SqlCommandBuilder builder = new SqlCommandBuilder(adapter);

                // Create a data Set object  
                DataSet ds = new DataSet("PersonSet");
                adapter.Fill(ds, "Person1");

                // Create a data table object and add a new row  
                DataTable PersonTable = ds.Tables["Person1"];
                DataRow row = PersonTable.NewRow();
                row["PersonID"] = "8";
                row["PersonName"] = "Ram";
                row["DateOfBirth"] = "2002/12/21";
                PersonTable.Rows.Add(row);

                // update data adapter  
                adapter.Update(ds, "Person1");
                MessageBox.Show(row["PersonID"].ToString().Trim() + " "
                + row["PersonName"].ToString().Trim() + " Added to Table Person");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadKey();
            }
            finally
            {
                con.Close();
            }
        }
        }
}
